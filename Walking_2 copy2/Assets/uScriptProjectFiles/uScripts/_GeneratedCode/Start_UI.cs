//uScript Generated Code - Build 1.0.2991
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Start_UI : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_10_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_10_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_11_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_11_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_12_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_12_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_15_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_15_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_20_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_21_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_21_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_22_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_25_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_25_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_26_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.AudioClip local_29_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_3_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_3_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_32_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.AudioClip local_35_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_38_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_38_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_39_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_39_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_40_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_40_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_41_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_41_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_42_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_42_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_51_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_51_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_52_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_52_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_53_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_53_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_56_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_56_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_63_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_63_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_64_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_64_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_65_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_65_UnityEngine_GameObject_previous = null;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_2 = "act1_cutscene_1";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_2 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_2 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_Quit logic_uScriptAct_Quit_uScriptAct_Quit_5 = new uScriptAct_Quit( );
   bool logic_uScriptAct_Quit_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_7 = "act1_cutscene_3";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_7 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_7 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_9 = "act1_cutscene_2";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_9 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_9 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_14 = "act1_cutscene_4";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_14 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_14 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_14 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_17 = "dialog1";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_17 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_17 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_17 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_18 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_18 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_18;
   bool logic_uScriptAct_LoadAudioClip_Out_18 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_19 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_19 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_23 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_23;
   bool logic_uScriptAct_LoadAudioClip_Out_23 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_24 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_24 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_27 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_27 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_27;
   bool logic_uScriptAct_LoadAudioClip_Out_27 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_28 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_28 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_28 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_30 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_30 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_30;
   bool logic_uScriptAct_LoadAudioClip_Out_30 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_31 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_31 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_31 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_33 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_33 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_33;
   bool logic_uScriptAct_LoadAudioClip_Out_33 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_34 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_34 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_34 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_36 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_36 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_36;
   bool logic_uScriptAct_LoadAudioClip_Out_36 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_37 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_37 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_37 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_44 = "dialog2";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_44 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_44 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_44 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_46 = "dialog3";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_46 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_46 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_46 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_48 = "dialog4";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_48 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_48 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_48 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_50 = "Walking_Mechanic";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_50 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_50 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_50 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_55 = "End2";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_55 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_55 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_58 = "End3";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_58 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_58 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_58 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_60 = "End4";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_60 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_60 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_60 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_62 = "Start_scene";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_62 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_62 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_62 = true;
   
   //event nodes
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = GameObject.Find( "PlayGame" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
      if ( null == local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_3_UnityEngine_GameObject = GameObject.Find( "Exit" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_4;
               }
            }
         }
      }
      if ( null == local_10_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_10_UnityEngine_GameObject = GameObject.Find( "cutscene1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_10_UnityEngine_GameObject_previous != local_10_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_10_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_10_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_8;
               }
            }
         }
         
         local_10_UnityEngine_GameObject_previous = local_10_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_10_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_10_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_10_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_8;
               }
            }
         }
      }
      if ( null == local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_11_UnityEngine_GameObject = GameObject.Find( "cutscene2" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_11_UnityEngine_GameObject_previous != local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_11_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_6;
               }
            }
         }
         
         local_11_UnityEngine_GameObject_previous = local_11_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_11_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_11_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_6;
               }
            }
         }
      }
      if ( null == local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_12_UnityEngine_GameObject = GameObject.Find( "cutscene3" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_12_UnityEngine_GameObject_previous != local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_12_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_12_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_13;
               }
            }
         }
         
         local_12_UnityEngine_GameObject_previous = local_12_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_12_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_12_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_12_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_13;
               }
            }
         }
      }
      if ( null == local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_15_UnityEngine_GameObject = GameObject.Find( "cutscene4" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_15_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_15_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_16;
               }
            }
         }
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_15_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_15_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_15_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_16;
               }
            }
         }
      }
      if ( null == local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_21_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_25_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_25_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_25_UnityEngine_GameObject_previous != local_25_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_25_UnityEngine_GameObject_previous = local_25_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_38_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_39_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_40_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_41_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_42_UnityEngine_GameObject = GameObject.Find( "di1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_42_UnityEngine_GameObject_previous != local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_42_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_43;
               }
            }
         }
         
         local_42_UnityEngine_GameObject_previous = local_42_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_42_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_42_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_43;
               }
            }
         }
      }
      if ( null == local_51_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_51_UnityEngine_GameObject = GameObject.Find( "di2" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_51_UnityEngine_GameObject_previous != local_51_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_51_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_51_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_45;
               }
            }
         }
         
         local_51_UnityEngine_GameObject_previous = local_51_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_51_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_51_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_51_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_45;
               }
            }
         }
      }
      if ( null == local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_52_UnityEngine_GameObject = GameObject.Find( "di3" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_52_UnityEngine_GameObject_previous != local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_52_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_52_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_47;
               }
            }
         }
         
         local_52_UnityEngine_GameObject_previous = local_52_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_52_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_52_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_52_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_47;
               }
            }
         }
      }
      if ( null == local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_53_UnityEngine_GameObject = GameObject.Find( "di4" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_53_UnityEngine_GameObject_previous != local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_53_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_49;
               }
            }
         }
         
         local_53_UnityEngine_GameObject_previous = local_53_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_53_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_53_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_49;
               }
            }
         }
      }
      if ( null == local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_56_UnityEngine_GameObject = GameObject.Find( "end1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_56_UnityEngine_GameObject_previous != local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_56_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_54;
               }
            }
         }
         
         local_56_UnityEngine_GameObject_previous = local_56_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_56_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_56_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_54;
               }
            }
         }
      }
      if ( null == local_63_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_63_UnityEngine_GameObject = GameObject.Find( "end2" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_63_UnityEngine_GameObject_previous != local_63_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_63_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_63_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_57;
               }
            }
         }
         
         local_63_UnityEngine_GameObject_previous = local_63_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_63_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_63_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_63_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_57;
               }
            }
         }
      }
      if ( null == local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_64_UnityEngine_GameObject = GameObject.Find( "end3" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_64_UnityEngine_GameObject_previous != local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_64_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_59;
               }
            }
         }
         
         local_64_UnityEngine_GameObject_previous = local_64_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_64_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_64_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_59;
               }
            }
         }
      }
      if ( null == local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_65_UnityEngine_GameObject = GameObject.Find( "end4" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_65_UnityEngine_GameObject_previous != local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_65_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_61;
               }
            }
         }
         
         local_65_UnityEngine_GameObject_previous = local_65_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_65_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_65_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_61;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_4;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_10_UnityEngine_GameObject_previous != local_10_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_10_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_10_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_8;
               }
            }
         }
         
         local_10_UnityEngine_GameObject_previous = local_10_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_10_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_10_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_10_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_8;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_11_UnityEngine_GameObject_previous != local_11_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_11_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_6;
               }
            }
         }
         
         local_11_UnityEngine_GameObject_previous = local_11_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_11_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_11_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_6;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_12_UnityEngine_GameObject_previous != local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_12_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_12_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_13;
               }
            }
         }
         
         local_12_UnityEngine_GameObject_previous = local_12_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_12_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_12_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_12_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_13;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_15_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_15_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_16;
               }
            }
         }
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_15_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_15_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_15_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_16;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_25_UnityEngine_GameObject_previous != local_25_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_25_UnityEngine_GameObject_previous = local_25_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_42_UnityEngine_GameObject_previous != local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_42_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_43;
               }
            }
         }
         
         local_42_UnityEngine_GameObject_previous = local_42_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_42_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_42_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_43;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_51_UnityEngine_GameObject_previous != local_51_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_51_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_51_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_45;
               }
            }
         }
         
         local_51_UnityEngine_GameObject_previous = local_51_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_51_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_51_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_51_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_45;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_52_UnityEngine_GameObject_previous != local_52_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_52_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_52_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_47;
               }
            }
         }
         
         local_52_UnityEngine_GameObject_previous = local_52_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_52_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_52_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_52_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_47;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_53_UnityEngine_GameObject_previous != local_53_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_53_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_49;
               }
            }
         }
         
         local_53_UnityEngine_GameObject_previous = local_53_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_53_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_53_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_49;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_56_UnityEngine_GameObject_previous != local_56_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_56_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_54;
               }
            }
         }
         
         local_56_UnityEngine_GameObject_previous = local_56_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_56_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_56_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_54;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_63_UnityEngine_GameObject_previous != local_63_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_63_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_63_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_57;
               }
            }
         }
         
         local_63_UnityEngine_GameObject_previous = local_63_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_63_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_63_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_63_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_57;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_64_UnityEngine_GameObject_previous != local_64_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_64_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_59;
               }
            }
         }
         
         local_64_UnityEngine_GameObject_previous = local_64_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_64_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_64_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_59;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_65_UnityEngine_GameObject_previous != local_65_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_65_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_61;
               }
            }
         }
         
         local_65_UnityEngine_GameObject_previous = local_65_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_65_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_65_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_61;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_1_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_0;
            }
         }
      }
      if ( null != local_3_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_4;
            }
         }
      }
      if ( null != local_10_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_10_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_8;
            }
         }
      }
      if ( null != local_11_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_11_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_6;
            }
         }
      }
      if ( null != local_12_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_12_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_13;
            }
         }
      }
      if ( null != local_15_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_15_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_16;
            }
         }
      }
      if ( null != local_42_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_43;
            }
         }
      }
      if ( null != local_51_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_51_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_45;
            }
         }
      }
      if ( null != local_52_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_52_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_47;
            }
         }
      }
      if ( null != local_53_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_53_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_49;
            }
         }
      }
      if ( null != local_56_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_56_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_54;
            }
         }
      }
      if ( null != local_63_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_63_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_57;
            }
         }
      }
      if ( null != local_64_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_64_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_59;
            }
         }
      }
      if ( null != local_65_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_65_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_61;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.SetParent(g);
      logic_uScriptAct_Quit_uScriptAct_Quit_5.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_18.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_27.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_30.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_33.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_36.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded += uScriptAct_LoadLevel_Loaded_2;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded += uScriptAct_LoadLevel_Loaded_7;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Loaded += uScriptAct_LoadLevel_Loaded_9;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14.Loaded += uScriptAct_LoadLevel_Loaded_14;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17.Loaded += uScriptAct_LoadLevel_Loaded_17;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Finished += uScriptAct_PlayAudioSource_Finished_19;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Finished += uScriptAct_PlayAudioSource_Finished_24;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Finished += uScriptAct_PlayAudioSource_Finished_28;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Finished += uScriptAct_PlayAudioSource_Finished_31;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Finished += uScriptAct_PlayAudioSource_Finished_34;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Finished += uScriptAct_PlayAudioSource_Finished_37;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44.Loaded += uScriptAct_LoadLevel_Loaded_44;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46.Loaded += uScriptAct_LoadLevel_Loaded_46;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Loaded += uScriptAct_LoadLevel_Loaded_48;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50.Loaded += uScriptAct_LoadLevel_Loaded_50;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Loaded += uScriptAct_LoadLevel_Loaded_55;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58.Loaded += uScriptAct_LoadLevel_Loaded_58;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Loaded += uScriptAct_LoadLevel_Loaded_60;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62.Loaded += uScriptAct_LoadLevel_Loaded_62;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded -= uScriptAct_LoadLevel_Loaded_2;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded -= uScriptAct_LoadLevel_Loaded_7;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Loaded -= uScriptAct_LoadLevel_Loaded_9;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14.Loaded -= uScriptAct_LoadLevel_Loaded_14;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17.Loaded -= uScriptAct_LoadLevel_Loaded_17;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Finished -= uScriptAct_PlayAudioSource_Finished_19;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Finished -= uScriptAct_PlayAudioSource_Finished_24;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Finished -= uScriptAct_PlayAudioSource_Finished_28;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Finished -= uScriptAct_PlayAudioSource_Finished_31;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Finished -= uScriptAct_PlayAudioSource_Finished_34;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Finished -= uScriptAct_PlayAudioSource_Finished_37;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44.Loaded -= uScriptAct_LoadLevel_Loaded_44;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46.Loaded -= uScriptAct_LoadLevel_Loaded_46;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.Loaded -= uScriptAct_LoadLevel_Loaded_48;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50.Loaded -= uScriptAct_LoadLevel_Loaded_50;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.Loaded -= uScriptAct_LoadLevel_Loaded_55;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58.Loaded -= uScriptAct_LoadLevel_Loaded_58;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.Loaded -= uScriptAct_LoadLevel_Loaded_60;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62.Loaded -= uScriptAct_LoadLevel_Loaded_62;
   }
   
   void Instance_OnButtonClick_0(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_0( );
   }
   
   void Instance_OnButtonClick_4(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_4( );
   }
   
   void Instance_OnButtonClick_6(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_6( );
   }
   
   void Instance_OnButtonClick_8(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_8( );
   }
   
   void Instance_OnButtonClick_13(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_13( );
   }
   
   void Instance_OnButtonClick_16(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_16( );
   }
   
   void Instance_OnButtonClick_43(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_43( );
   }
   
   void Instance_OnButtonClick_45(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_45( );
   }
   
   void Instance_OnButtonClick_47(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_47( );
   }
   
   void Instance_OnButtonClick_49(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_49( );
   }
   
   void Instance_OnButtonClick_54(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_54( );
   }
   
   void Instance_OnButtonClick_57(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_57( );
   }
   
   void Instance_OnButtonClick_59(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_59( );
   }
   
   void Instance_OnButtonClick_61(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_61( );
   }
   
   void uScriptAct_LoadLevel_Loaded_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_2( );
   }
   
   void uScriptAct_LoadLevel_Loaded_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_7( );
   }
   
   void uScriptAct_LoadLevel_Loaded_9(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_9( );
   }
   
   void uScriptAct_LoadLevel_Loaded_14(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_14( );
   }
   
   void uScriptAct_LoadLevel_Loaded_17(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_17( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_19(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_19( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_24(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_24( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_28(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_28( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_31(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_31( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_34(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_34( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_37(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_37( );
   }
   
   void uScriptAct_LoadLevel_Loaded_44(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_44( );
   }
   
   void uScriptAct_LoadLevel_Loaded_46(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_46( );
   }
   
   void uScriptAct_LoadLevel_Loaded_48(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_48( );
   }
   
   void uScriptAct_LoadLevel_Loaded_50(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_50( );
   }
   
   void uScriptAct_LoadLevel_Loaded_55(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_55( );
   }
   
   void uScriptAct_LoadLevel_Loaded_58(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_58( );
   }
   
   void uScriptAct_LoadLevel_Loaded_60(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_60( );
   }
   
   void uScriptAct_LoadLevel_Loaded_62(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_62( );
   }
   
   void Relay_OnButtonClick_0()
   {
      if (true == CheckDebugBreak("99c93805-3fdd-48c8-89f5-0ffdd1a1730c", "UI_Button_Events", Relay_OnButtonClick_0)) return; 
      Relay_In_18();
   }
   
   void Relay_Loaded_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("98517908-a1a6-4b84-9cb6-5ec3728a0b30", "Load_Level", Relay_Loaded_2)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("98517908-a1a6-4b84-9cb6-5ec3728a0b30", "Load_Level", Relay_In_2)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.In(logic_uScriptAct_LoadLevel_name_2, logic_uScriptAct_LoadLevel_destroyOtherObjects_2, logic_uScriptAct_LoadLevel_blockUntilLoaded_2);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_4()
   {
      if (true == CheckDebugBreak("cf27f18e-b428-4b5e-b896-52bc6c61c673", "UI_Button_Events", Relay_OnButtonClick_4)) return; 
      Relay_In_23();
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("538251e4-ee2f-42a3-bfa9-253c647959ff", "Quit", Relay_In_5)) return; 
         {
         }
         logic_uScriptAct_Quit_uScriptAct_Quit_5.In();
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Quit.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_6()
   {
      if (true == CheckDebugBreak("d9c8cc77-b311-4ca9-945a-d9fd69b8444a", "UI_Button_Events", Relay_OnButtonClick_6)) return; 
      Relay_In_30();
   }
   
   void Relay_Loaded_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d9ebe943-5db1-4e21-bfb8-1864bb75e9a3", "Load_Level", Relay_Loaded_7)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d9ebe943-5db1-4e21-bfb8-1864bb75e9a3", "Load_Level", Relay_In_7)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.In(logic_uScriptAct_LoadLevel_name_7, logic_uScriptAct_LoadLevel_destroyOtherObjects_7, logic_uScriptAct_LoadLevel_blockUntilLoaded_7);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_8()
   {
      if (true == CheckDebugBreak("eb44e20d-70a9-4b87-a62d-ae0784a76963", "UI_Button_Events", Relay_OnButtonClick_8)) return; 
      Relay_In_27();
   }
   
   void Relay_Loaded_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("de4009e9-0f0a-4395-b35e-7b0ba9ce4542", "Load_Level", Relay_Loaded_9)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("de4009e9-0f0a-4395-b35e-7b0ba9ce4542", "Load_Level", Relay_In_9)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.In(logic_uScriptAct_LoadLevel_name_9, logic_uScriptAct_LoadLevel_destroyOtherObjects_9, logic_uScriptAct_LoadLevel_blockUntilLoaded_9);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_13()
   {
      if (true == CheckDebugBreak("632af52d-86c2-479a-81bf-65fdc783dac8", "UI_Button_Events", Relay_OnButtonClick_13)) return; 
      Relay_In_33();
   }
   
   void Relay_Loaded_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6ed29ab5-ec73-42f7-a441-fd28b6014a7a", "Load_Level", Relay_Loaded_14)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6ed29ab5-ec73-42f7-a441-fd28b6014a7a", "Load_Level", Relay_In_14)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_14.In(logic_uScriptAct_LoadLevel_name_14, logic_uScriptAct_LoadLevel_destroyOtherObjects_14, logic_uScriptAct_LoadLevel_blockUntilLoaded_14);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_16()
   {
      if (true == CheckDebugBreak("e46e59a0-7eb4-4422-8904-9c500c03fee7", "UI_Button_Events", Relay_OnButtonClick_16)) return; 
      Relay_In_36();
   }
   
   void Relay_Loaded_17()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("800ac496-7c53-4da7-9435-174577a620ed", "Load_Level", Relay_Loaded_17)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_17()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("800ac496-7c53-4da7-9435-174577a620ed", "Load_Level", Relay_In_17)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_17.In(logic_uScriptAct_LoadLevel_name_17, logic_uScriptAct_LoadLevel_destroyOtherObjects_17, logic_uScriptAct_LoadLevel_blockUntilLoaded_17);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_18()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("294f31da-1fe8-4d20-8060-046db89f1913", "Load_AudioClip", Relay_In_18)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_18.In(logic_uScriptAct_LoadAudioClip_name_18, out logic_uScriptAct_LoadAudioClip_audioClip_18);
         local_20_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_18;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_18.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_19();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Finished_19)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Play_19)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_21_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_19 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_19 = local_20_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Play(logic_uScriptAct_PlayAudioSource_target_19, logic_uScriptAct_PlayAudioSource_audioClip_19);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Out;
         
         if ( test_0 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Stop_19)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_21_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_19 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_19 = local_20_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Stop(logic_uScriptAct_PlayAudioSource_target_19, logic_uScriptAct_PlayAudioSource_audioClip_19);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_19.Out;
         
         if ( test_0 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_23()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f510257b-f59e-498c-9713-53f2735eccc5", "Load_AudioClip", Relay_In_23)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.In(logic_uScriptAct_LoadAudioClip_name_23, out logic_uScriptAct_LoadAudioClip_audioClip_23);
         local_22_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_23;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_24();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Finished_24)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Play_24)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_25_UnityEngine_GameObject_previous != local_25_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_25_UnityEngine_GameObject_previous = local_25_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_25_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_24 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_24 = local_22_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Play(logic_uScriptAct_PlayAudioSource_target_24, logic_uScriptAct_PlayAudioSource_audioClip_24);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Stop_24)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_25_UnityEngine_GameObject_previous != local_25_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_25_UnityEngine_GameObject_previous = local_25_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_25_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_24 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_24 = local_22_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Stop(logic_uScriptAct_PlayAudioSource_target_24, logic_uScriptAct_PlayAudioSource_audioClip_24);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f4c55323-d90d-4bcf-9dc4-2e48ebe4d002", "Load_AudioClip", Relay_In_27)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_27.In(logic_uScriptAct_LoadAudioClip_name_27, out logic_uScriptAct_LoadAudioClip_audioClip_27);
         local_26_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_27;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_27.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_28();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_28()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Finished_28)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_28()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Play_28)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_38_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_28 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_28 = local_26_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Play(logic_uScriptAct_PlayAudioSource_target_28, logic_uScriptAct_PlayAudioSource_audioClip_28);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_28()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Stop_28)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_38_UnityEngine_GameObject_previous != local_38_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_38_UnityEngine_GameObject_previous = local_38_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_38_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_28 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_28 = local_26_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Stop(logic_uScriptAct_PlayAudioSource_target_28, logic_uScriptAct_PlayAudioSource_audioClip_28);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_28.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("30db3ee1-01b5-4b6a-9d1c-c1acac4187a7", "Load_AudioClip", Relay_In_30)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_30.In(logic_uScriptAct_LoadAudioClip_name_30, out logic_uScriptAct_LoadAudioClip_audioClip_30);
         local_29_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_30;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_30.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_31();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_31()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Finished_31)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_31()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Play_31)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_39_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_31 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_31 = local_29_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Play(logic_uScriptAct_PlayAudioSource_target_31, logic_uScriptAct_PlayAudioSource_audioClip_31);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_31()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Stop_31)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_39_UnityEngine_GameObject_previous != local_39_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_39_UnityEngine_GameObject_previous = local_39_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_39_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_31 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_31 = local_29_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Stop(logic_uScriptAct_PlayAudioSource_target_31, logic_uScriptAct_PlayAudioSource_audioClip_31);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_31.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_33()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("2bb3b7c5-4d81-44cc-b87e-0455a25446c4", "Load_AudioClip", Relay_In_33)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_33.In(logic_uScriptAct_LoadAudioClip_name_33, out logic_uScriptAct_LoadAudioClip_audioClip_33);
         local_32_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_33;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_33.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_34();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_34()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Finished_34)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_34()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Play_34)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_40_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_34 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_34 = local_32_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Play(logic_uScriptAct_PlayAudioSource_target_34, logic_uScriptAct_PlayAudioSource_audioClip_34);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Out;
         
         if ( test_0 == true )
         {
            Relay_In_14();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_34()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Stop_34)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_40_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_34 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_34 = local_32_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Stop(logic_uScriptAct_PlayAudioSource_target_34, logic_uScriptAct_PlayAudioSource_audioClip_34);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_34.Out;
         
         if ( test_0 == true )
         {
            Relay_In_14();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_36()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("661a6c7a-800c-4e04-af00-ce7088ecf2b5", "Load_AudioClip", Relay_In_36)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_36.In(logic_uScriptAct_LoadAudioClip_name_36, out logic_uScriptAct_LoadAudioClip_audioClip_36);
         local_35_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_36;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_36.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_37();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_37()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Finished_37)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_37()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Play_37)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_41_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_37 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_37 = local_35_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Play(logic_uScriptAct_PlayAudioSource_target_37, logic_uScriptAct_PlayAudioSource_audioClip_37);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Out;
         
         if ( test_0 == true )
         {
            Relay_In_17();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_37()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Stop_37)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_41_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_37 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_37 = local_35_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Stop(logic_uScriptAct_PlayAudioSource_target_37, logic_uScriptAct_PlayAudioSource_audioClip_37);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_37.Out;
         
         if ( test_0 == true )
         {
            Relay_In_17();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_43()
   {
      if (true == CheckDebugBreak("a63c7911-2e30-4c7c-bb60-71baed0e3095", "UI_Button_Events", Relay_OnButtonClick_43)) return; 
      Relay_In_44();
   }
   
   void Relay_Loaded_44()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e416582-1875-466d-91a0-a2f2e7e81974", "Load_Level", Relay_Loaded_44)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_44()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e416582-1875-466d-91a0-a2f2e7e81974", "Load_Level", Relay_In_44)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_44.In(logic_uScriptAct_LoadLevel_name_44, logic_uScriptAct_LoadLevel_destroyOtherObjects_44, logic_uScriptAct_LoadLevel_blockUntilLoaded_44);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_45()
   {
      if (true == CheckDebugBreak("28ad9ba0-ce0e-4a1b-a71d-0ceaaa1010d6", "UI_Button_Events", Relay_OnButtonClick_45)) return; 
      Relay_In_46();
   }
   
   void Relay_Loaded_46()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("80895b46-ef6f-4626-9ecd-af8d2e500aed", "Load_Level", Relay_Loaded_46)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_46()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("80895b46-ef6f-4626-9ecd-af8d2e500aed", "Load_Level", Relay_In_46)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_46.In(logic_uScriptAct_LoadLevel_name_46, logic_uScriptAct_LoadLevel_destroyOtherObjects_46, logic_uScriptAct_LoadLevel_blockUntilLoaded_46);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_47()
   {
      if (true == CheckDebugBreak("4f107563-1704-49a5-a803-a96abb42b69e", "UI_Button_Events", Relay_OnButtonClick_47)) return; 
      Relay_In_48();
   }
   
   void Relay_Loaded_48()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("79a3eeaf-671f-4320-8a29-098876047dd3", "Load_Level", Relay_Loaded_48)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_48()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("79a3eeaf-671f-4320-8a29-098876047dd3", "Load_Level", Relay_In_48)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_48.In(logic_uScriptAct_LoadLevel_name_48, logic_uScriptAct_LoadLevel_destroyOtherObjects_48, logic_uScriptAct_LoadLevel_blockUntilLoaded_48);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_49()
   {
      if (true == CheckDebugBreak("27f54d66-5912-454a-b866-2505f5900f59", "UI_Button_Events", Relay_OnButtonClick_49)) return; 
      Relay_In_50();
   }
   
   void Relay_Loaded_50()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0e6fbcd8-4d74-472f-aa6a-d777865f7c4a", "Load_Level", Relay_Loaded_50)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_50()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0e6fbcd8-4d74-472f-aa6a-d777865f7c4a", "Load_Level", Relay_In_50)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_50.In(logic_uScriptAct_LoadLevel_name_50, logic_uScriptAct_LoadLevel_destroyOtherObjects_50, logic_uScriptAct_LoadLevel_blockUntilLoaded_50);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_54()
   {
      if (true == CheckDebugBreak("8be92c03-7bce-4c7a-ab9a-d03390b1aa72", "UI_Button_Events", Relay_OnButtonClick_54)) return; 
      Relay_In_55();
   }
   
   void Relay_Loaded_55()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("58b5d60e-d838-4b06-8b7b-e9df09e1183f", "Load_Level", Relay_Loaded_55)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_55()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("58b5d60e-d838-4b06-8b7b-e9df09e1183f", "Load_Level", Relay_In_55)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_55.In(logic_uScriptAct_LoadLevel_name_55, logic_uScriptAct_LoadLevel_destroyOtherObjects_55, logic_uScriptAct_LoadLevel_blockUntilLoaded_55);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_57()
   {
      if (true == CheckDebugBreak("c5d7e395-dbb9-489e-9887-eb593915e5fd", "UI_Button_Events", Relay_OnButtonClick_57)) return; 
      Relay_In_58();
   }
   
   void Relay_Loaded_58()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b5e720c0-13fc-47e3-adae-83e8b17aee21", "Load_Level", Relay_Loaded_58)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_58()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b5e720c0-13fc-47e3-adae-83e8b17aee21", "Load_Level", Relay_In_58)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_58.In(logic_uScriptAct_LoadLevel_name_58, logic_uScriptAct_LoadLevel_destroyOtherObjects_58, logic_uScriptAct_LoadLevel_blockUntilLoaded_58);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_59()
   {
      if (true == CheckDebugBreak("58d4d53a-b6b3-473e-bd66-1f643fdde5d9", "UI_Button_Events", Relay_OnButtonClick_59)) return; 
      Relay_In_60();
   }
   
   void Relay_Loaded_60()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9b3f0f0c-9d05-49e9-89b2-73ef4545ae78", "Load_Level", Relay_Loaded_60)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_60()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9b3f0f0c-9d05-49e9-89b2-73ef4545ae78", "Load_Level", Relay_In_60)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_60.In(logic_uScriptAct_LoadLevel_name_60, logic_uScriptAct_LoadLevel_destroyOtherObjects_60, logic_uScriptAct_LoadLevel_blockUntilLoaded_60);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_61()
   {
      if (true == CheckDebugBreak("14a7b19f-1274-483b-b06d-9086da1a42d0", "UI_Button_Events", Relay_OnButtonClick_61)) return; 
      Relay_In_62();
   }
   
   void Relay_Loaded_62()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("76d18580-333e-47aa-ae75-fd121fd2dfc0", "Load_Level", Relay_Loaded_62)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_62()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("76d18580-333e-47aa-ae75-fd121fd2dfc0", "Load_Level", Relay_In_62)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_62.In(logic_uScriptAct_LoadLevel_name_62, logic_uScriptAct_LoadLevel_destroyOtherObjects_62, logic_uScriptAct_LoadLevel_blockUntilLoaded_62);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:1", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "1ff697c8-c049-4ba7-8ab3-c5cf833bcf9e", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:3", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ed4a4692-2c3e-459a-bbe4-a8a56b316d36", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:10", local_10_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ce99ba67-8c0f-4806-b455-8585b593fe00", local_10_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:11", local_11_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "4aefca76-7e08-46e1-9155-92844c239a4e", local_11_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:12", local_12_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c78537fe-998d-4305-8478-440acbc6b744", local_12_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:15", local_15_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "936d2f75-df46-4a9d-bd82-d93892b516f5", local_15_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:20", local_20_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5aa0147b-2a06-4c85-b21c-ab8490b356df", local_20_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:21", local_21_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "21bc2018-4a1d-457f-b80c-a222165ca497", local_21_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:22", local_22_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "99def98a-333b-4c9a-b3df-d96ab1ec60c2", local_22_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:25", local_25_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "444b5af3-a497-4384-aeab-7c6ef6b99d31", local_25_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:26", local_26_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ae78b5f8-7182-4eff-ac26-ffe0931e156c", local_26_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:29", local_29_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "82469434-485a-4efb-a741-f0074ab187e1", local_29_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:32", local_32_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "22c04771-43a6-4ead-b7ac-95de8a49f4b1", local_32_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:35", local_35_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "74bb6279-20a5-4978-834a-787018dbdce9", local_35_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:38", local_38_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e8b76a9d-36d2-4d49-a4d8-643db3aa329e", local_38_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:39", local_39_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a7ae5d5c-7794-4ba0-b12f-b906b0016933", local_39_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:40", local_40_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "12e56009-d445-40f6-8e49-c0bff4f12bfd", local_40_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:41", local_41_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "dd78b7b8-d15f-4b7c-a6b5-2eb2afbaeeff", local_41_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:42", local_42_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8750307b-e0c4-465e-985d-ad60b93a4039", local_42_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:51", local_51_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "fae4d618-65cc-44f3-b809-026010051b38", local_51_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:52", local_52_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8481ddb4-12bb-4347-bd3b-b5ec6d52c51a", local_52_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:53", local_53_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a4b38341-9c97-4e34-aff1-c82b69f746c2", local_53_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:56", local_56_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "06788745-4be6-4094-be97-a6b643a9a865", local_56_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:63", local_63_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "767dc716-16eb-4b91-b0ec-36abf01b626f", local_63_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:64", local_64_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c8b0f0fd-200f-483e-b854-17026fa2b956", local_64_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:65", local_65_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "09e72cf3-0854-41e2-b3aa-61c15578be98", local_65_UnityEngine_GameObject);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
