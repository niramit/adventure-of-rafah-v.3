﻿using UnityEngine;
using System.Collections;

public class Snake : MonoBehaviour,IMonster  {

	private int Health;
	public Animator useAnimator;
	public Rigidbody2D rb;
	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		Health = 4;
	}

	// Update is called once per frame
	void Update () 
	{

	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", true);
		} 
	}
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", false);
		} 
	}
	public void hit()
	{
		print ("hit");
		Health--;
		if (Health == 0) 
		{
			Destroy (this.gameObject, 0.0f);
		} 
	}
}
