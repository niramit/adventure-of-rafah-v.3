﻿using UnityEngine;
using System.Collections;

public class GoatAttack : MonoBehaviour {
	public Animator useAnimator;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", true);
		} 
	}
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", false);
		} 
	}

}
