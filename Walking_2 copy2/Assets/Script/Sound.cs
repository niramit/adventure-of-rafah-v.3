﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour 
{
	public AudioClip WalkingSound;
	public AudioClip RunningSound;
	private AudioSource source;


	void Start() 
	{
		source = GetComponent<AudioSource> ();

	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.LeftArrow) ||
			Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			source.clip = WalkingSound;
			source.Play ();
		} 
		if (Input.GetKeyUp (KeyCode.LeftArrow) ||
			Input.GetKeyUp (KeyCode.RightArrow)) 
		{
			source.Stop ();
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.LeftShift) ||
				Input.GetKeyDown (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.LeftShift))
		{
			source.clip = RunningSound;
			source.Play ();
		} 
		if (Input.GetKeyUp (KeyCode.LeftArrow) && Input.GetKeyUp (KeyCode.LeftShift) ||
			Input.GetKeyUp (KeyCode.RightArrow) && Input.GetKeyUp (KeyCode.LeftShift))
		{
			source.Stop ();
		}
			
	}		

	}
		


		
		



