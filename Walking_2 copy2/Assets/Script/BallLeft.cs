﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallLeft : MonoBehaviour 
{

	public Rigidbody2D rb;
	public float speed;
	public int LionB;
	public GameObject BallTextUI;
	public GameObject Lionball;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		rb.AddForce (new Vector2 (speed,0));
		Destroy (this.gameObject, 3);
		LionB = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnCollisionbEnter2D (Collision2D other)
	{
	if (other.gameObject.CompareTag ("punch")) 
		{
			Debug.Log ("get ball yeah");
			Destroy (this.gameObject);
		}	
	}
}
