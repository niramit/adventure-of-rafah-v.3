﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {

	public Animator useAnimator;
	public int count ;
	void Update () 
	{
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				useAnimator.SetBool ("Iswalkl", true);
			} else {
				useAnimator.SetBool ("Iswalkl", false);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
			} else if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrunl", true);
			} else {
				useAnimator.SetBool ("Isrunl", false);
			}

			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				useAnimator.SetBool ("Iswalk", true);
			} else {
				useAnimator.SetBool ("Iswalk", false);
			}

			if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
			} else if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isrun", true);
			} else {
				useAnimator.SetBool ("Isrun", false);
			}

			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.Space) || Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
			if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjump", true);
			}

			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.Space) || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.Space) && Input.GetKey (KeyCode.LeftShift)) {
				useAnimator.SetBool ("Isjumpl", true);
			}
			
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjumpl", true);
			} 
			if (Input.GetKeyDown (KeyCode.Space)) {
				useAnimator.SetBool ("Isjump", true);
			}
			
		if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.X) && count == 0|| Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.X)&&count == 0) {
				useAnimator.SetBool ("Ispunch", true);
			}

		if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.X) || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.X)&&count == 0) {
				useAnimator.SetBool ("Ispunchl", true);
			}

		if (Input.GetKeyDown (KeyCode.X)&&count == 0) {
				useAnimator.SetBool ("Ispunchl", true);
			} 
		if (Input.GetKeyDown (KeyCode.X)&&count == 0) {
				useAnimator.SetBool ("Ispunch", true);
			} else {
				useAnimator.SetBool ("Ispunch", false);
				useAnimator.SetBool ("Ispunchl", false);
			}

		if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.X)&& count == 1 || Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.X)&& count == 1) {
			useAnimator.SetBool ("Isslash", true);
		}

		if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.X)&& count == 1 || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.X)&& count == 1) {
			useAnimator.SetBool ("Isslashl", true);
		}

		if (Input.GetKeyDown (KeyCode.X)&& count == 1) {
			useAnimator.SetBool ("Isslashl", true);
		} 
		if (Input.GetKeyDown (KeyCode.X)&& count == 1) {
			useAnimator.SetBool ("Isslash", true);
		} else {
			useAnimator.SetBool ("Isslash", false);
			useAnimator.SetBool ("Isslashl", false);
		}


		if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.X)&& count == 2 || Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.X)&& count == 2) {
			useAnimator.SetBool ("Isslash2", true);
		}

		if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.X)&& count == 2 || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.X)&& count == 2) {
			useAnimator.SetBool ("Isslashl2", true);
		}

		if (Input.GetKeyDown (KeyCode.X)&& count == 2) {
			useAnimator.SetBool ("Isslashl2", true);
		} 
		if (Input.GetKeyDown (KeyCode.X)&& count == 2) {
			useAnimator.SetBool ("Isslash2", true);
		} else {
			useAnimator.SetBool ("Isslash2", false);
			useAnimator.SetBool ("Isslashl2", false);
		}

	}
	void OnCollisionEnter2D(Collision2D col)
	{
			if (col.gameObject.CompareTag ("ground") || col.gameObject.CompareTag ("spike") || col.gameObject.CompareTag ("moveItem")) {
				this.useAnimator.SetBool ("Isjump", false);
			}
				if (col.gameObject.CompareTag ("ground") || col.gameObject.CompareTag ("spike") || col.gameObject.CompareTag ("moveItem")) {
				this.useAnimator.SetBool ("Isjumpl", false);
			}

		if (col.gameObject.CompareTag ("bamboo")) {
			count = count + 1;
		}
	}
	void OnTriggerStay2D(Collider2D coll)
	{
			if (coll.gameObject.CompareTag ("moveItem") && Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.Z) || coll.gameObject.CompareTag ("moveItem") && Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Z)) {
				this.useAnimator.SetBool ("Ispush", true);
			} else {
				this.useAnimator.SetBool ("Ispush", false);
			}

		if (coll.gameObject.CompareTag ("moveItem") && Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.Z) || coll.gameObject.CompareTag ("moveItem") && Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Z)) {
			this.useAnimator.SetBool ("Ispushl", true);
		} else {
			this.useAnimator.SetBool ("Ispushl", false);
		}

	}
}
