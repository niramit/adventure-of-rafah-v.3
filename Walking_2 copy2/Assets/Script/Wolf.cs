﻿using UnityEngine;
using System.Collections;

public class Wolf : MonoBehaviour {

	// Use this for initialization
	public Rigidbody2D rb;
	private float s;
	public Animator useAnimator;



	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = -4;
	}

	// Update is called once per frame
	void Update () 
	{
		this.transform.Translate (new Vector3 (s, 0, 0) * Time.deltaTime);
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		if (other.gameObject.CompareTag ("Player")) {
			useAnimator.SetBool ("hit", true);
		}
	}
	void OnCollisionExit2D(Collision2D other)
	{
		useAnimator.SetBool ("hit", false);
	}

	public void hit()
	{
		print ("hit");
		Destroy (this.gameObject, 0.0f);
	}
}
