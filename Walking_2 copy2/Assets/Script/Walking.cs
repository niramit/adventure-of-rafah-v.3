﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Walking : MonoBehaviour 
	{
	public Rigidbody2D rb;
	//public Animator useAnimator;
	public float gunTime;
	public float swordTime;
	public float punchTime;
	public float Health;
	public float CurrentHealth;
	public int DeathCount;
	public GameObject PunchUI;
	public GameObject healthBar;
	public GameObject munPrefab;
	public GameObject munLaunchPoint;
	//public GameObject swordLaunchPoint;
	//public GameObject swordPrefab;
	public GameObject punchLaunchPoint;
	public GameObject punchPrefab;
	public Transform cameraPosi; 
	public GameObject mainCamera;
	public Animator useAnimator;
	private GameObject Player;
	public int Planks;
	public GameObject PlankText;
	public int LionB;
	public GameObject Lionball;
	//private int WeaponCount;
	//private bool canFire;
	//private bool canSlash;
	private bool canjump;
	private bool canpunch;
	private string charDirection;

	// Use this for initialization
		void Start () 
		{
			rb = GetComponent<Rigidbody2D>();
			//canFire = true;
			//canSlash = true;
			canjump = true;
			canpunch = true;
			DeathCount = 1;
			charDirection = "right";
			Planks = 0;
			LionB = 0;
			//WeaponCount = 0;
		}
	
		// Update is called once per frame
		void Update () 
		{
		if (!PauseController.isPause) {
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				//this.transform.Translate (new Vector3 (-6, 0, 0) * Time.deltaTime);
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (-4, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -8) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (4, 0);
				}
			}

			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				if (charDirection != "left") {
					charDirection = "left";
					punchLaunchPoint.transform.Translate (new Vector3 (-3.2f, 0, 0));
				}
			}
			
			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) {
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (-7, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -14) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (7, 0);
				}
			}




			if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.LeftShift)) {
				//this.transform.Translate (new Vector3 (-6, 0, 0) * Time.deltaTime);
				if (charDirection != "left") {
					charDirection = "left";
					punchLaunchPoint.transform.Translate (new Vector3 (-3.2f, 0, 0));
					//canpunch = false;
				}
			}
			
			if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) {
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (-7, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -14) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (7, 0);
				}
			}

			if (Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.LeftShift)) {
				//this.transform.Translate (new Vector3 (-6, 0, 0) * Time.deltaTime);
				if (charDirection != "left") {
					charDirection = "left";
					punchLaunchPoint.transform.Translate (new Vector3 (-3.2f, 0, 0));
				}
			}
			 
			
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				//this.transform.Translate (new Vector3 (5, 0, 0) * Time.deltaTime);
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (4, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 8) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (4, 0);
				}
				if (charDirection != "right") {
					charDirection = "right";
					punchLaunchPoint.transform.Translate (new Vector3 (3.2f, 0, 0));
					//canpunch = false;
				}

			}
			 

			if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.LeftShift)) {
				//this.transform.Translate (new Vector3 (6, 0, 0) * Time.deltaTime);
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (7, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 14) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (7, 0);
				}
				if (charDirection != "right") {
					charDirection = "right";
					punchLaunchPoint.transform.Translate (new Vector3 (3.2f, 0, 0));
				} 
			} else if (Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftShift)) {
				//this.transform.Translate (new Vector3 (6, 0, 0) * Time.deltaTime);
				this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (7, 0);
				if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 14) {
					this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (7, 0);
				}
				if (charDirection != "right") {
					charDirection = "right";
					punchLaunchPoint.transform.Translate (new Vector3 (3.2f, 0, 0));
				} 
			}
			if (Input.GetKeyDown (KeyCode.Space) && canjump) {
				rb.AddForce (new Vector2 (0, 475));
				canjump = false;
			}
			/*if (Input.GetKeyDown(KeyCode.X) && canFire) 
			{
				GameObject munObject=Instantiate(munPrefab,munLaunchPoint.transform.position,Quaternion.identity) as GameObject;
				munObject.name = "Player mun";
				canFire = false;
				StartCoroutine(gunWait());
			} */
			//if (WeaponCount == 1) 
			//{
				//if (Input.GetKeyDown (KeyCode.X) && canSlash)
				//{
					//GameObject swordObject = Instantiate (swordPrefab, swordLaunchPoint.transform.position, Quaternion.identity) as GameObject;
					//swordObject.name = "Player sword";
					//canSlash = false;
					//StartCoroutine (swordWait ());
				//}
			//}

			//if (WeaponCount == 0) 
			//{
				if (Input.GetKeyDown (KeyCode.X) && canpunch) 
				{
					GameObject swordObject = Instantiate (punchPrefab, punchLaunchPoint.transform.position, Quaternion.identity) as GameObject;
					swordObject.name = "Player punch";
					canpunch = false;
					StartCoroutine (punchWait ());
				}
			//}
		}
		}
		/*IEnumerator gunWait()
		{
			yield return new WaitForSeconds(gunTime);
			canFire = true;
		}*/
		//IEnumerator swordWait()
		//{
			//yield return new WaitForSeconds(swordTime);
			//canSlash = true;
		//}
		IEnumerator punchWait()
		{
			yield return new WaitForSeconds(punchTime);
			canpunch = true;
		}
		void OnCollisionEnter2D(Collision2D other)
		{
			if (other.gameObject.CompareTag ("bamboo")) 
			{
				Destroy (other.gameObject);
				Destroy (PunchUI.gameObject);
				//WeaponCount += 1;
			}
			if (other.gameObject.CompareTag ("monsters")||other.gameObject.CompareTag ("spike"))
			{
				RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
				healthRect.sizeDelta -= new Vector2 ( 35 , 0);
				DeathCount++;
			if (DeathCount > 10) {
				Application.LoadLevel (Application.loadedLevelName);
			}
			}

		if (other.gameObject.CompareTag ("ground")||other.gameObject.CompareTag ("spike")||other.gameObject.CompareTag ("moveItem"))
			{
				canjump = true;
			}
			if (other.gameObject.CompareTag ("Item"))
			{
				if(DeathCount > 2)
				{
					RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
					healthRect.sizeDelta += new Vector2 ( 70 , 0);
					DeathCount--;
					DeathCount--;
				}
				else if(DeathCount == 2)
				{
					RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
					healthRect.sizeDelta += new Vector2 ( 35 , 0);
					DeathCount--;
				}
				Destroy (other.gameObject, 0.1f);
			}
			if (other.gameObject.CompareTag ("load"))
			{
				Application.LoadLevel (Application.loadedLevelName);
			}

		if (other.gameObject.CompareTag ("Planks")) 
		{
			Planks = Planks + 1;
			Destroy (other.gameObject);
			Debug.Log ("get plank");


			if (Planks == 1) 
			{
				Debug.Log ("Plank = 1");
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "1 / 4";
			} 
			else if (Planks == 2) 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "2 / 4";
			} 
			else if (Planks == 3) 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "3 / 4";
			} 
			else 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "4 / 4";
			}
		}
			
	}
			

		/*if (other.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.RightArrow)&&Input.GetKey (KeyCode.Z)||other.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.D)&&Input.GetKey (KeyCode.Z))
			{
				other.transform.Translate (new Vector3 (8, 0, 0) * Time.deltaTime);
			}*/

		
	void OnTriggerStay2D(Collider2D coll) 
	{
		if (coll.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.RightArrow)&&Input.GetKey (KeyCode.Z)||coll.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.D)&&Input.GetKey (KeyCode.Z))
		{
			print ("hit");
			coll.transform.Translate (new Vector3 (3, 0, 0) * Time.deltaTime);
		}
		if (coll.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.LeftArrow)&&Input.GetKey (KeyCode.Z)||coll.gameObject.CompareTag ("moveItem")&&Input.GetKey (KeyCode.A)&&Input.GetKey (KeyCode.Z))
		{
			print ("hit");
			coll.transform.Translate (new Vector3 (-3, 0, 0) * Time.deltaTime);
		}
	}
		
		
}



// Set active for Dialog mechanics
// Decrease amount of polygon cillider
// Use add force to that goddamn rock by Prach hahahahahaha+ haha ha ha ...ne na na CENA!!!! na nana naaaa