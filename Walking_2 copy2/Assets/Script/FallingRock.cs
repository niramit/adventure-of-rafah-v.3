﻿using UnityEngine;
using System.Collections;

public class FallingRock : MonoBehaviour 
{
	public GameObject DogMonster;
	public GameObject SoHinBoss;
	public GameObject Door;
	public GameObject DoorOpen;
	public GameObject Hin;
	public GameObject Wood;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			this.transform.Translate (new Vector3 (0, -20, 0) * Time.deltaTime);
			DoorOpen.transform.Translate (new Vector3 (0, 59, 0));
			Hin.transform.Translate (new Vector3 (0, 33, 0));
			Wood.transform.Translate (new Vector3 (0, 33, 0));
			SoHinBoss.transform.Translate (new Vector3 (0, -10000, 0) * Time.deltaTime);
			Destroy(DogMonster.gameObject, 0.1f);
			Destroy(Door.gameObject, 0.1f);
		}
	}
}
