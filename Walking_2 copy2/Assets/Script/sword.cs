﻿using UnityEngine;
using System.Collections;

public class sword : MonoBehaviour {

	//public Rigidbody2D rb;
		// Use this for initialization
		void Start () 
		{
			//rb = GetComponent<Rigidbody2D>();
			Destroy (this.gameObject, 0.2f);
		}

		// Update is called once per frame
		void Update () {
			//transform.Translate(new Vector3(1,3,0) * Time.deltaTime, Space.World);
			transform.Rotate(new Vector3(0,0,-270) * Time.deltaTime, Space.World);
		}

		void OnCollisionEnter2D(Collision2D other)
		{
			IMonster mon;
			if (other.gameObject.GetComponent(typeof(IMonster)))
			{
			mon = other.gameObject.GetComponent(typeof(IMonster)) as IMonster;
			mon.hit ();
			}
		}

	}
