﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueInLevel : MonoBehaviour 
{
	
	//public GameObject dialog1;
	//public GameObject dialog2;
	//public GameObject dialog3;
	public GameObject dialog4;
	public GameObject dialog5;
	public GameObject dialog6;
	public GameObject dialog7;
	public GameObject dialog8;
	public GameObject dialog9;
	public GameObject dialog10;

	public GameObject HealthUI;

	private bool IsControlled;

	void Start () 
	{
		//dialog1.SetActive (false);
		//dialog2.SetActive (false);
		//dialog3.SetActive (false);
		dialog4.SetActive (false);
		dialog5.SetActive (false);
		dialog6.SetActive (false);
		dialog7.SetActive (false);
		dialog8.SetActive (false);
		dialog9.SetActive (false);
		dialog10.SetActive (false);

		IsControlled = true;
	}

	IEnumerator DialogCountdown()
	{
		IsControlled = false;
		yield return new WaitForSeconds (5.0f);
		dialog4.SetActive (false);
		dialog5.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog5.SetActive (false);
		dialog6.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog6.SetActive (false);
		dialog7.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog7.SetActive (false);
		dialog8.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog8.SetActive (false);
		dialog9.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog9.SetActive (false);
		dialog10.SetActive (true);
		yield return new WaitForSeconds (5.0f);
		dialog10.SetActive (false);
		HealthUI.SetActive (true);
		PauseController.isPause = false;
		Destroy (this.gameObject);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			print ("DialogueIsPlaying");
			PauseController.isPause = true;
			dialog4.SetActive (true);
			HealthUI.SetActive (false);
			StartCoroutine (DialogCountdown());
		}
	}
}
