﻿using UnityEngine;
using System.Collections;

public class munnnn : MonoBehaviour {
	public Rigidbody2D rb;
	public float speed;
	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		rb.AddForce (new Vector2 (speed, 0));
		Destroy (this.gameObject, 1);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		IMonster mon;
		if (other.gameObject.GetComponent(typeof(IMonster)))
		{
			mon = other.gameObject.GetComponent(typeof(IMonster)) as IMonster;
			mon.hit ();
		}

		if (other.gameObject.CompareTag ("rock"))
		{
			Destroy (this.gameObject, 0);
		}
		if (other.gameObject.CompareTag ("monsters"))
		{
			//Destroy (other.gameObject, 0);
			Destroy (this.gameObject, 0.05f);
			// monster die animation
			//GameObject munObject=Instantiate(munPrefab,munLaunchPoint.transform.position,Quaternion.identity) as GameObject;
			//munObject.name = "Player mun";

		}
	}

}
