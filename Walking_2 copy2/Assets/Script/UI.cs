﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour 
{
	public Mask HealthMask;
	public float Health;

	public void Damage( float value)
	{
		Health -= value;
		HealthMask.transform.localScale += new Vector3 (-10, 0, 0);
	}

	// Update is called once per frame
	void Update ()
	{
		
	}

}