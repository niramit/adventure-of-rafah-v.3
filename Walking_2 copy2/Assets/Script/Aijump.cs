﻿using UnityEngine;
using System.Collections;

public class Aijump : MonoBehaviour,IMonster {
	public Rigidbody2D rb;
	public float s;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		s = 20;
	}
	
	// Update is called once per frame
	void Update () {
		rb.AddForce(new Vector2(0,s));
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("upDown") )
		{
			s *= -1;
		}
	}
	public void hit()
	{
		print ("hit");
		Destroy (this.gameObject, 0.0f);
	}
}

