﻿using UnityEngine;
using System.Collections;

public class SomjedWalk : MonoBehaviour, IMonster
{
	public Rigidbody2D rb;
	private float s;
	//public Animator useAnimator;
	private int GhostHealth;



	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = -2;
		GhostHealth = 3;
	}

	// Update is called once per frame
	void Update () 
	{

		this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (s, 0);
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -4) 
		{
			this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (4, 0);
		}
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 4) {
			this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (4, 0);
		}

	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		//if (other.gameObject.CompareTag ("Player")) {
			//useAnimator.SetBool ("hit", true);
		//} else {
		//	useAnimator.SetBool ("hit", false);
		//}
	}

	public void hit()
	{
		GhostHealth--;
		if (GhostHealth == 0) 
		{
			Destroy (this.gameObject, 0.0f);
		}
	}
}
