﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LionJadeMechanic : MonoBehaviour 
{
	public GameObject BallLeft;
	private bool BallActive;
	private float gunTime = 1.0f;
	public GameObject BallLauncher;
	public Text TimerText;
	public float MyCoolTimer = 60;

	void Start () 
	{
		BallActive = true;
		//TimerText = GetComponent<Text> ();
	}

	void Update () 
	{
	}

	void OnCollisionStay2D (Collision2D other)
	{
		if (other.gameObject.CompareTag ("ground")&& BallActive == true) 
		{
			GameObject munObject=Instantiate(BallLeft,BallLauncher.transform.position,Quaternion.identity) as GameObject;
			munObject.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine(gunWait());

			//MyCoolTimer -= Time.deltaTime;
			//TimerText.text = MyCoolTimer.ToString ("f0"); 
			//print (MyCoolTimer);
		}
		if (other.gameObject.CompareTag ("ground")) 
		{
			Debug.Log ("StartCountTime");
			GameObject.Find ("CountdownText").GetComponent<Text> ().text = "Time Remaining : " + MyCoolTimer;
			MyCoolTimer -= Time.deltaTime;
			TimerText.text = MyCoolTimer.ToString ("f0"); 
		}
	}

	IEnumerator gunWait()
	{
			yield return new WaitForSeconds(gunTime);
			BallActive = true;
	}




}
