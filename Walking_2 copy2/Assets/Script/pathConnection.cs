﻿using UnityEngine;
using System.Collections;

public class pathConnection : MonoBehaviour {
	public Rigidbody2D rb;
	private float s;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		s = -8;
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity =  new Vector3 (s, 0, 0);
		//transform.Translate(new Vector3(s,0,0) * Time.deltaTime);
	}
	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next")) {
			s *= -1;
		}
		if (other.gameObject.CompareTag("Player")){
			other.transform.Translate(new Vector3(s/10,0,0) * Time.deltaTime);
		}
	}
}
