﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountdownLionTimer : MonoBehaviour 
{

	public Text TimerText;
	public float MyCoolTimer = 60;

	void Start () 
	{
		TimerText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		MyCoolTimer -= Time.deltaTime;
		TimerText.text = MyCoolTimer.ToString ("f0"); 
		print (MyCoolTimer);
	}

}
